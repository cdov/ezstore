package edu.mum.ezstore.config;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.cors.CorsConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


/**
 * @author cdov
 */
public class WebapiCorsConfig extends CorsConfiguration {
    private static final Logger log = LoggerFactory.getLogger(WebapiCorsConfig.class);
    private final List<Pattern> allowedOriginsRegex;

    public WebapiCorsConfig(CorsConfigProperties corsConfigProperties) {
        allowedOriginsRegex = new ArrayList<>();
        setAllowCredentials(corsConfigProperties.isAllowCredentials());
        setAllowedHeaders(corsConfigProperties.getAllowHeaders());
        setAllowedMethods(corsConfigProperties.getAllowMethods());
        setMaxAge(corsConfigProperties.getMaxAge());
        corsConfigProperties.getAllowOrigins().stream()
                .filter(StringUtils::isNotEmpty)
                .forEach(this::addAllowedOrigin);
    }

    @Override
    public void addAllowedOrigin(String origin) {
        super.addAllowedOrigin(origin);
        try {
            allowedOriginsRegex.add(Pattern.compile(origin));
        }
        catch(PatternSyntaxException e) {
            log.warn("Invalid regex syntax for the origin {}. Ignore it if it is not supposed to be a regex expression.", origin);
        }
    }

    @Override
    public String checkOrigin(String requestOrigin) {
        String result = super.checkOrigin(requestOrigin);
        return result != null ? result : checkOriginWithRegex(requestOrigin);
    }

    protected String checkOriginWithRegex(String requestOrigin) {
        return allowedOriginsRegex.stream()
                .anyMatch(pattern -> pattern.matcher(requestOrigin).lookingAt())
                ? requestOrigin : null;
    }

}
