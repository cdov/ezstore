package edu.mum.ezstore.config.rest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.net.URI;



//@JsonInclude(Include.NON_EMPTY) //for Jackson 2.x
@Data
public class RestError implements Serializable {
	/**
	 * An absolute URI that identifies the problem type. When dereferenced, it
	 * SHOULD provide human-readable documentation for the problem type (e.g.,
	 * using HTML). When this member is not present, its value is assumed to be
	 * "about:blank".
	 */
	private URI type;

	/**
	 * The HTTP status code generated by the origin server for this occurrence
	 * of the problem.
	 */
	private int httpCode;
	/**
	 * A short, human-readable summary of the problem type. It SHOULD NOT change
	 * from occurrence to occurrence of the problem, except for purposes of
	 * localization.
	 */
	private String title;

	private Integer errorCode;
	private String message;
	private String diagnostic;
	@JsonIgnore
	private String messageOriginal;
	@JsonIgnore
	private String diagnosticOriginal;

	public RestError() {
	
	}
	public RestError(RestError error) {
		super();
		this.type = error.getType();
		this.httpCode = error.getHttpCode();
		this.title = error.getTitle();
		this.errorCode = error.getErrorCode();
		this.message = error.getMessage();
		this.diagnostic = error.getDiagnostic();
	}

	public void setHttpCode(HttpStatus httpCode) {
		this.httpCode = httpCode.value();
	}

}
