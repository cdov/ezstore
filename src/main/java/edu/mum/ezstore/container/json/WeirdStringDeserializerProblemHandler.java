package edu.mum.ezstore.container.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.DeserializationProblemHandler;
import com.google.common.base.Defaults;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Component
public class WeirdStringDeserializerProblemHandler extends DeserializationProblemHandler {

    private final JacksonMappingErrorHolder jacksonMappingErrorHolder;

    @Autowired
    public WeirdStringDeserializerProblemHandler(JacksonMappingErrorHolder jacksonMappingErrorHolder) {
        this.jacksonMappingErrorHolder = jacksonMappingErrorHolder;
    }

    @Override
    public Object handleUnexpectedToken(DeserializationContext ctxt, Class<?> targetType, JsonToken t, JsonParser p, String failureMsg) throws IOException {
        if(StringUtils.isBlank(failureMsg)){
            failureMsg = "Can not deserialize instance of " + targetType.getName() + " out of " + t.name() + " token";
        }
        return handleDeserializerProblem(ctxt, targetType, p.getText(), failureMsg);
    }

    @Override
    public Object handleWeirdStringValue(DeserializationContext ctxt, Class<?> targetType, String valueToConvert, String failureMsg) throws IOException {
        return handleDeserializerProblem(ctxt, targetType, valueToConvert, failureMsg);
    }

    private Object handleDeserializerProblem(DeserializationContext ctxt, Class<?> targetType, String valueToConvert, String failureMsg) throws IOException {
        String field = ctxt.getParser().getCurrentName();
        String fieldPath = getFieldPath(ctxt.getParser());
        if(StringUtils.isNotBlank(fieldPath)){
            field = fieldPath.concat(".").concat(field);
        }
        jacksonMappingErrorHolder.addError(field, failureMsg, valueToConvert);
        return targetType.isPrimitive() ? Defaults.defaultValue(targetType) : null;
    }
    private String getFieldPath(JsonParser jsonParser) {
        List<String> path = Lists.newArrayList();
        JsonStreamContext context = jsonParser.getParsingContext().getParent();

        do {
            if (context.inArray()) {
                path.add("[" + context.getCurrentIndex() + "]");
            }
            if (StringUtils.isNoneBlank(context.getCurrentName())) {
                path.add(context.getCurrentName());
            }
            context = context.getParent();
        } while (context != null);

        Collections.reverse(path);
        String fullPath = StringUtils.join(path, ".");
        return StringUtils.replace(fullPath, ".[", "[");
    }
}
