package edu.mum.ezstore.controller.user;

import edu.mum.ezstore.domain.User;
import edu.mum.ezstore.shared.BaseTestController;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Sam on 1/21/2017.
 */
class UserControllerTest extends BaseTestController {
    private final String END_POINT="/user";

    @Test
    @WithMockUser("spring")
    void testGetAllUser() throws Exception{
        this.mockMvc.perform(get(END_POINT+"/getall")).andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.*", hasSize(greaterThan(0)) ));
    }

    @Test
    @WithMockUser("spring")
    void testAdduser() throws Exception{
        final User request = new User();
        request.setFirstName("Test");
        request.setLastName("some description");

        mockMvc.perform(post(END_POINT+"/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(stringify(request))

        ).andDo(print()).andExpect(status().isUnprocessableEntity())
        ;
    }

}
