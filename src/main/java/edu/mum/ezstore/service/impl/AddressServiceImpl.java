package edu.mum.ezstore.service.impl;

import com.egen.exhandle.exception.ObjectNotFoundException;
import edu.mum.ezstore.aspect.annotation.AnnotationValidation;
import edu.mum.ezstore.domain.Address;
import edu.mum.ezstore.repository.AddressRepository;
import edu.mum.ezstore.service.AddressService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AddressServiceImpl implements AddressService {

	private final AddressRepository addressRepository;

	public AddressServiceImpl(AddressRepository addressRepository) {
		this.addressRepository = addressRepository;
	}

	public List<Address> findAll(){
		return addressRepository.findAll();
	}
	
	@AnnotationValidation
	public Address save(Address address){
		return addressRepository.save(address);
	}

	public Address findOne(Long id){
		return addressRepository.findById(id).orElseThrow(()-> new ObjectNotFoundException("Address Id: " + id));
	}
}
