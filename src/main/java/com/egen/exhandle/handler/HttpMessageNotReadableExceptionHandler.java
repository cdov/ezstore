/*
 * Copyright 2014 Jakub Jirutka <jakub@jirutka.cz>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.egen.exhandle.handler;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import edu.mum.ezstore.config.rest.RestError;
import edu.mum.ezstore.config.rest.ValidationRestError;
import org.springframework.http.converter.HttpMessageNotReadableException;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

public class HttpMessageNotReadableExceptionHandler extends ErrorMessageRestExceptionHandler<HttpMessageNotReadableException> {


    public HttpMessageNotReadableExceptionHandler() {
        super(BAD_REQUEST);
    }

    @Override
    public RestError createBody(HttpMessageNotReadableException ex, HttpServletRequest req) {

        RestError tmpl = super.createBody(ex, req);
        ValidationRestError msg = new ValidationRestError(tmpl);

        if (ex.getCause() instanceof InvalidFormatException) {
            InvalidFormatException invalidFormatException = (InvalidFormatException) ex.getCause();
            String field = invalidFormatException.getPath().stream().
                    map(JsonMappingException.Reference::getFieldName).
                    collect(Collectors.joining("."));
            msg.addError(field, invalidFormatException.getValue(), invalidFormatException.getOriginalMessage());
        } else if (ex.getCause() instanceof JsonMappingException) {
            JsonMappingException jsonMappingException = (JsonMappingException) ex.getCause();
            String field = jsonMappingException.getPath().stream().
                    map(JsonMappingException.Reference::getFieldName).
                    collect(Collectors.joining("."));
            msg.addError(field, null, jsonMappingException.getOriginalMessage());
        } else {
            return tmpl;
        }

        return msg;
    }


}
