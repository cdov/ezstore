package edu.mum.ezstore.service.impl;

import com.egen.exhandle.exception.BusinessException;
import com.egen.exhandle.exception.ObjectNotFoundException;
import edu.mum.ezstore.aspect.annotation.AnnotationValidation;
import edu.mum.ezstore.domain.Comment;
import edu.mum.ezstore.domain.User;
import edu.mum.ezstore.repository.CommentRepository;
import edu.mum.ezstore.service.CommentService;
import edu.mum.ezstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {

	@Autowired
	CommentRepository commentRepository;
	
	@Autowired
	UserService userService;
	
	public List<Comment> findAll(){
		return commentRepository.findAll();
	}

	@AnnotationValidation
	public Comment save(Comment comment){
		User toUser =  userService.findOne(comment.getToUser().getId()) ;
		comment.setToUser(toUser);

		//insert the comment date
		Date date = new Date();
		comment.setDate(date);

		return commentRepository.save(comment);
	}

	@AnnotationValidation
	public Comment update(Comment comment){
		User toUser =  userService.findOne(comment.getToUser().getId()) ;
		comment.setToUser(toUser);

		// check if comment is existed
		Comment oldComment = commentRepository.findById(comment.getId()).orElseThrow(()->new ObjectNotFoundException("Comment Id: " + comment.getId()));

		// check if toUser is changed or not
		if (oldComment.getToUser().getId() != comment.getToUser().getId()){
			throw new BusinessException("Cannot change ToUser to another user");
	}

		//insert the comment date
		Date date = new Date();
		comment.setDate(date);

		return commentRepository.save(comment);
	}

	public Comment findOne(Long id){
		return commentRepository.findById(id).orElseThrow(()->new ObjectNotFoundException("Comment Id: " + id));
	}
	
}
