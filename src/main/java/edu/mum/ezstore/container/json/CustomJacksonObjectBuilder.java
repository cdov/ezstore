package edu.mum.ezstore.container.json;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.deser.DeserializationProblemHandler;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.text.SimpleDateFormat;

@Configuration
public class CustomJacksonObjectBuilder implements Jackson2ObjectMapperBuilderCustomizer {

    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    private final DeserializationProblemHandler deserializationProblemHandler;

    @Autowired
    public CustomJacksonObjectBuilder(DeserializationProblemHandler deserializationProblemHandler) {
        this.deserializationProblemHandler = deserializationProblemHandler;
    }


    @Override
    public void customize(Jackson2ObjectMapperBuilder builder) {
        Module module = new MyModule(deserializationProblemHandler);

        builder.dateFormat(formatter);
        builder.modulesToInstall(module);
    }

    private static class MyModule extends SimpleModule {
        private final transient DeserializationProblemHandler deserializationProblemHandler;

        private MyModule(DeserializationProblemHandler deserializationProblemHandler) {
            this.deserializationProblemHandler = deserializationProblemHandler;
        }

        @Override
        public void setupModule(SetupContext context) {
            // Required, as documented in the Javadoc of SimpleModule
            super.setupModule(context);
            context.addDeserializationProblemHandler(deserializationProblemHandler);
        }
    }
}
