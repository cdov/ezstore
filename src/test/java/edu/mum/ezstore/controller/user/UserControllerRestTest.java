package edu.mum.ezstore.controller.user;

import edu.mum.ezstore.shared.BaseRestController;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;

/**
 * Created by Sam on 1/24/2017.
 */
class UserControllerRestTest extends BaseRestController{
    private final String END_POINT="/user";

    @Test
    void testGetAllUser() throws Exception{
        String auth=getPassword("sammy","123456");
        given().
            header("Authorization", auth).
        //log().all(). //log the request detail
        when().
            get(END_POINT+"/getall").
        then().
            log().all(). //log the result
            statusCode(200).
            body("size()",greaterThan(0));
    }
    @Test
    void testUnAuthenticateRequest() throws Exception{
        given().
        when().
            get(END_POINT+"/getall").
        then().
            log().ifError(). //log if error happens
            statusCode(401).
            body("httpCode",equalTo(401)).
            body("title",equalTo("UnAuthentication"));
    }
}
