package edu.mum.ezstore.config;

import edu.mum.ezstore.config.rest.RestJsonExceptionResolver;
import edu.mum.ezstore.filter.CacheControlHeaderFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;

import java.util.List;



@Configuration
public class MvcConfig implements WebMvcConfigurer {

    private final CorsConfigProperties corsConfigProperties;

    public MvcConfig(CorsConfigProperties corsConfigProperties) {
        this.corsConfigProperties = corsConfigProperties;
    }

    /** Register RestJsonExceptionResolver */
	@Override
    public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
		// resolves @ExceptionHandler first
		exceptionResolvers.add(exceptionHandlerExceptionResolver() ); 
		// then resolves using restJsonExceptionResolver
		exceptionResolvers.add(restJsonExceptionResolver());
    }

	@Bean
    public RestJsonExceptionResolver restJsonExceptionResolver() {
	    RestJsonExceptionResolver bean = new RestJsonExceptionResolver(httpErrorMessageSource());
	    // map any spring and servlet exception class here	   
        bean.setOrder(100);
	    return bean;
    }
	@Bean
    public ExceptionHandlerExceptionResolver exceptionHandlerExceptionResolver() {
        ExceptionHandlerExceptionResolver resolver = new ExceptionHandlerExceptionResolver();
//        resolver.setMessageConverters(HttpMessageConverterUtils.getDefaultHttpMessageConverters());
        return resolver;
    }

	@Bean
    public MessageSource httpErrorMessageSource() {
        ReloadableResourceBundleMessageSource m = new ReloadableResourceBundleMessageSource();
        m.setBasename("classpath:/messages/httpErrorMessages");
        m.setDefaultEncoding("UTF-8");
        return m;
    }

    @Bean
    @ConditionalOnProperty(value = "ezstore.cors.enabled", havingValue = "true")
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new WebapiCorsConfig(corsConfigProperties));
        return new CorsFilter(source);
    }

    @Bean
    public FilterRegistrationBean<CacheControlHeaderFilter> cacheControlFilter(){
        FilterRegistrationBean<CacheControlHeaderFilter> registrationBean
                = new FilterRegistrationBean<>();

        registrationBean.setFilter(new CacheControlHeaderFilter());
        registrationBean.addUrlPatterns("/**");

        return registrationBean;
    }
}