package edu.mum.ezstore.validator;

import com.egen.exhandle.exception.DomainValidationException;
import com.google.common.collect.Lists;
import edu.mum.ezstore.config.rest.CustomFieldError;
import edu.mum.ezstore.container.json.JacksonMappingErrorHolder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;

import javax.validation.constraints.NotNull;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import static com.google.common.base.CaseFormat.LOWER_CAMEL;
import static com.google.common.base.CaseFormat.LOWER_UNDERSCORE;

/**
 * constructor DI is recommend by spring team
 * http://olivergierke.de/2013/11/why-field-injection-is-evil/
 * example of using lombok for constructor DI
 */
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AnnotationValidator {

    private final @NotNull Validator validator;

    private final @NotNull MessageSourceAccessor messageAccessor;

    private final @NotNull JacksonMappingErrorHolder jacksonMappingErrorHolder;

    public void doValidate(Object object) {
        System.out.println();
        System.out.println("DOING Validation! Object:" + object.getClass().getName());
        //throw error here, don't want to combine the error with spring validate
//        if(jacksonMappingErrorHolder.hasErrors()){
//            throw new DomainValidationException(jacksonMappingErrorHolder.getErrors());
//        }

        Set<CustomFieldError> customFieldErrors=jacksonMappingErrorHolder.getErrors();
        Errors errors = new BeanPropertyBindingResult(object, object.getClass().getName());
        validator.validate(object, errors);

        Validator customValidator = ValidatorFactory.getInstance().getValidator(object.getClass().getSimpleName());
        if (customValidator != null)
            customValidator.validate(object, errors);

        if (errors.hasErrors()) {
            List<FieldError> fieldErrors = errors.getFieldErrors();

            fieldErrors.forEach(fieldError -> {
                CustomFieldError customFieldError= new CustomFieldError(LOWER_CAMEL.to(LOWER_UNDERSCORE, fieldError.getField()), fieldError.getRejectedValue(), messageAccessor.getMessage(fieldError));
                customFieldErrors.add(customFieldError);
            });
//			for (FieldError fieldError : fieldErrors) {
//
//				// System.out.println(messageAccessor.getMessage(fieldError));
//			}
        }

        if(!customFieldErrors.isEmpty()){
            List<CustomFieldError> sortedErrors = Lists.newArrayList(customFieldErrors);
            sortedErrors.sort(Comparator.comparing(CustomFieldError::getField));
            throw new DomainValidationException(sortedErrors);
        }
    }

}
