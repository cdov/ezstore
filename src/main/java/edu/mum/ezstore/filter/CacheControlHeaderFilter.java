package edu.mum.ezstore.filter;

import edu.mum.ezstore.util.WebRequestUtil;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Sam on 4/19/2017.
 * this filter is use to disable cache to the client when no-cache in the request header
 */
public class CacheControlHeaderFilter implements Filter{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
//        Nothing to do
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if(!(servletRequest instanceof HttpServletRequest)){
            throw new ServletException("Error: servletRequest is not an instance of HttpServletRequest");
        }
        final HttpServletRequest httpServletRequest=(HttpServletRequest) servletRequest;

        if("no-cache".equalsIgnoreCase(WebRequestUtil.getRequestHeaderCacheControl(httpServletRequest))){
            if(!(servletResponse instanceof HttpServletResponse)){
                throw new ServletException("Error: servletResponse is not an instance of HttpServletResponse");
            }
            final HttpServletResponse httpServletResponse=(HttpServletResponse) servletResponse;

            httpServletResponse.setHeader("Cache-Control", "no-cache");

        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
//        Nothing to do
    }
}
