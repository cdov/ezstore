package edu.mum.ezstore.repository;

import edu.mum.ezstore.domain.UserCredentials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCredentialRepository extends JpaRepository<UserCredentials, String>{
	
}
