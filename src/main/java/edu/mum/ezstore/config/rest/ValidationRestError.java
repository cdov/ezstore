package edu.mum.ezstore.config.rest;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.CaseFormat.LOWER_CAMEL;
import static com.google.common.base.CaseFormat.LOWER_UNDERSCORE;


//@JsonInclude(Include.NON_EMPTY) //for Jackson 2.x
public class ValidationRestError extends RestError {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private List<CustomFieldError> errors = new ArrayList<>(6);


    public ValidationRestError(RestError orig) {
        super(orig);
    }

    public ValidationRestError(RestError orig, List<CustomFieldError> errors) {
        super(orig);
        this.errors = errors;
    }

    public ValidationRestError addError(String field, Object rejectedValue, String message) {
        CustomFieldError error = new CustomFieldError(LOWER_CAMEL.to(LOWER_UNDERSCORE, field), rejectedValue, message);

        errors.add(error);
        return this;
    }

    public ValidationRestError addError(String message) {
        CustomFieldError error = new CustomFieldError();
        error.setMessage(message);

        errors.add(error);
        return this;
    }

    private String toSnakeCase(String field){
        if(StringUtils.isNotBlank(field)){
            String regex = "([a-z])([A-Z])";
            String replacement = "$1_$2";
            field = field.replaceAll(regex, replacement).toLowerCase();
        }
        return field;
    }
}
