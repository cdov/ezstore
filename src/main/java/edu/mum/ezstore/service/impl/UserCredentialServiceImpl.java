package edu.mum.ezstore.service.impl;

import com.egen.exhandle.exception.ObjectNotFoundException;
import edu.mum.ezstore.aspect.annotation.AnnotationValidation;
import edu.mum.ezstore.domain.UserCredentials;
import edu.mum.ezstore.repository.UserCredentialsRepository;
import edu.mum.ezstore.service.UserCredentialService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserCredentialServiceImpl implements UserCredentialService {

	private final UserCredentialsRepository userCredentialsRepository;

	public UserCredentialServiceImpl(UserCredentialsRepository userCredentialsRepository) {
		this.userCredentialsRepository = userCredentialsRepository;
	}

	public List<UserCredentials> findAll() {
		return userCredentialsRepository.findAll();
	}

	@AnnotationValidation
	public UserCredentials save(UserCredentials usercredentials) {
		return userCredentialsRepository.save(usercredentials);
	}

	@Override
	public UserCredentials findByUserName(String username) {
		return userCredentialsRepository.findById(username).orElseThrow(()-> new ObjectNotFoundException("User credential: " + username));
	}
}
