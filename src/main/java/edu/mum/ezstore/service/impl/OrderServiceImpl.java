package edu.mum.ezstore.service.impl;

import com.egen.exhandle.exception.BusinessException;
import com.egen.exhandle.exception.ObjectNotFoundException;
import edu.mum.ezstore.aspect.annotation.AnnotationValidation;
import edu.mum.ezstore.domain.Item;
import edu.mum.ezstore.domain.ItemOrder;
import edu.mum.ezstore.domain.User;
import edu.mum.ezstore.repository.OrderRepository;
import edu.mum.ezstore.service.ItemService;
import edu.mum.ezstore.service.OrderService;
import edu.mum.ezstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class OrderServiceImpl implements OrderService{

	private final OrderRepository orderRepository;

	private final ItemService itemService;

	private final UserService userService;

	@Autowired
	public OrderServiceImpl(OrderRepository orderRepository, ItemService itemService, UserService userService) {
		this.orderRepository = orderRepository;
		this.itemService = itemService;
		this.userService = userService;
	}

	public List<ItemOrder> findAll(){
		return orderRepository.findAll();
	}

	@AnnotationValidation
	public ItemOrder save(ItemOrder itemOrder){

		// check if item is existed or not
		Item item = itemService.findOne(itemOrder.getItem().getId());

		// check if item is sold or not
		if (item.getItemOrder() != null) throw new BusinessException("Item is already sold");

		// set item to itemOrder
		itemOrder.setItem(item);
		// save
		ItemOrder savedOrder = orderRepository.save(itemOrder);

		item.setItemOrder(savedOrder);
		itemService.save(item);

		return savedOrder;
	}
	public ItemOrder findOne(Long orderID){
		return orderRepository.findById(orderID).orElseThrow(()-> new ObjectNotFoundException("orderId:"+ orderID));
	}

	@Override
	public List<ItemOrder> findByBuyer(User buyer) {
		return orderRepository.findByBuyer(buyer);
	}
}
