package edu.mum.ezstore.util;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Sam on 4/19/2017.
 */
public class WebRequestUtil {
    public final static String REQUEST_HEADER_ORIGIN = "origin";
    public final static String REQUEST_HEADER_HOST = "host";
    public final static String REQUEST_HEADER_REFERRER = "Referer";
    public final static String REQUEST_HEADER_CACHE_CONTROL = "Cache-Control";

    public static String getRequestHeaderOrigin(final HttpServletRequest request){
        return request.getHeader(REQUEST_HEADER_ORIGIN);
    }
    public static String getRequestHeaderHost(final HttpServletRequest request){
        return request.getHeader(REQUEST_HEADER_HOST);
    }
    public static String getRequestHeaderReferrer(final HttpServletRequest request){
        return request.getHeader(REQUEST_HEADER_REFERRER);
    }
    public static String getRequestHeaderCacheControl(final HttpServletRequest request){
        return request.getHeader(REQUEST_HEADER_CACHE_CONTROL);
    }
}
