package edu.mum.ezstore.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.convert.DurationUnit;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * @author cdov
 */
@Component
@ConfigurationProperties("ezstore.cors")
public class CorsConfigProperties {

    private List<String> allowOrigins = new ArrayList<>();
    private List<String> allowMethods;
    private List<String> allowHeaders;
    @DurationUnit(ChronoUnit.SECONDS)
    private Duration maxAge = Duration.ofHours(1);
    private boolean allowCredentials = true;

    public List<String> getAllowOrigins() {
        return allowOrigins;
    }

    public void setAllowOrigins(List<String> allowOrigins) {
        this.allowOrigins = allowOrigins;
    }

    public List<String> getAllowMethods() {
        return allowMethods;
    }

    public void setAllowMethods(List<String> allowMethods) {
        this.allowMethods = allowMethods;
    }

    public List<String> getAllowHeaders() {
        return allowHeaders;
    }

    public void setAllowHeaders(List<String> allowHeaders) {
        this.allowHeaders = allowHeaders;
    }

    public long getMaxAge() {
        return maxAge.getSeconds();
    }

    public void setMaxAge(Duration maxAge) {
        this.maxAge = maxAge;
    }

    public boolean isAllowCredentials() {
        return allowCredentials;
    }

    public void setAllowCredentials(boolean allowCredentials) {
        this.allowCredentials = allowCredentials;
    }
}
