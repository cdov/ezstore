package edu.mum.ezstore.shared;

import org.hibernate.validator.HibernateValidator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.fail;

/**
 * Created by Sam on 1/23/2017.
 */
public class DtoValidator<T> {
    private LocalValidatorFactoryBean localValidatorFactoryBean;
    private Set<ConstraintViolation<T>> errors;

    public DtoValidator(){
        localValidatorFactoryBean=new LocalValidatorFactoryBean();
        localValidatorFactoryBean.setProviderClass(HibernateValidator.class);
        localValidatorFactoryBean.afterPropertiesSet();
    }

    public Set<ConstraintViolation<T>> validate(final T object){
        errors = localValidatorFactoryBean.validate(object);
        return errors;
    }

    /**
     * assert that there is no errors for this object
     */
    public void assertNoErrors(){
        assertNoErrors(null);
    }

    /**
     * assert that there is no errors for this object with custom message
     * @param message custom message
     */
    public void assertNoErrors(String message){
        if(errors.size()==0){
            return;
        }
        fail(message==null?String.format("Errors found: %s",errorsToString()):message);
    }

    /**
     * assert there is no error for this field name
     * @param fieldName data member of domain
     */
    public void assertNotError(String fieldName){
        assertNotError(null,fieldName);
    }
    public void assertNotError(String message,String fieldName){
        if(!hasErrorField(fieldName)){
            return;
        }
        fail(message==null?String.format("Error '%s' found",fieldName):message);
    }

    /**
     * assert there is a error for this field name
     * @param fieldName
     */
    public void assertError(String fieldName){
        assertError(null,fieldName);
    }
    public void assertError(String message,String fieldName){
        if(hasErrorField(fieldName)){
            return;
        }
        fail(message==null?String.format("Error '%s' not found",fieldName):message);
    }

    /**
     * assert there's not a error with the following message
     * @param errorName error message
     */
    public void assertNotErrorMessage(String errorName){
        assertNotErrorMessage(null,errorName);
    }
    public void assertNotErrorMessage(String message,String errorName){
        if(!hasErrorMessage(errorName)){
            return;
        }
        fail(message==null?String.format("Error '%s' found",errorName):message);
    }

    /**
     * assert there's a error with the following message
     * @param errorName error message
     */
    public void assertErrorMessage(String errorName){
        assertErrorMessage(null,errorName);
    }
    public void assertErrorMessage(String message,String errorName){
        if(hasErrorMessage(errorName)){
            return;
        }
        fail(message==null?String.format("Error '%s' not found",errorName):message);
    }

    private boolean hasErrorMessage(final String errorName){
        for(ConstraintViolation<?> error:errors){
            if(error.getMessage().equals(errorName)){
                return true;
            }
        }
        return false;
    }
    private boolean hasErrorField(final String fieldName){
        for(ConstraintViolation<?> error:errors){
            if(error.getPropertyPath().toString().equals(fieldName)){
                return true;
            }
        }
        return false;
    }
    private String errorsToString(){
        final StringBuilder errorString=new StringBuilder();
        errorString.append('[');
        for(ConstraintViolation error:errors){
            if(errorString.length()>1){
                errorString.append(",");
            }
            errorString.append(error.getPropertyPath().toString()+":"+error.getMessage());
        }
        errorString.append(']');
        return errorString.toString();
    }
}
