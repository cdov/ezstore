package edu.mum.ezstore.container.json;

import com.google.common.collect.Sets;
import edu.mum.ezstore.config.rest.CustomFieldError;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * use to hold the invalid format error when deserialize from json
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class JacksonMappingErrorHolder {
    private Set<CustomFieldError> errors = Sets.newHashSet();

    public Set<CustomFieldError> getErrors() {
        return errors;
    }

    public void addError(String field, String message) {
        addError(field, message, null);
    }

    public void addError(String field, String message, Object reject) {
        addError(new CustomFieldError(field, reject, message));
    }

    public void addError(CustomFieldError error) {
        errors.add(error);
    }

    public boolean hasErrors() {
        return !errors.isEmpty();
    }

}
