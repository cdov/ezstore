package edu.mum.ezstore.service.impl;

import com.egen.exhandle.exception.ObjectNotFoundException;
import edu.mum.ezstore.aspect.annotation.AnnotationValidation;
import edu.mum.ezstore.domain.Category;
import edu.mum.ezstore.repository.CategoryRepository;
import edu.mum.ezstore.service.CategoryService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService{

	private final CategoryRepository categoryRepository;

	public CategoryServiceImpl(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}

	public List<Category> findAll(){
		return categoryRepository.findAll();
	}

	@AnnotationValidation
	public Category save(Category category){
		return categoryRepository.save(category);
	}

	public Category findOne(Long id){
		return categoryRepository.findById(id).orElseThrow(()-> new ObjectNotFoundException("Category Id: " + id));
	}
}
