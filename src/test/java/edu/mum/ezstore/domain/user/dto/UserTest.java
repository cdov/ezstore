package edu.mum.ezstore.domain.user.dto;

import edu.mum.ezstore.domain.Address;
import edu.mum.ezstore.domain.User;
import edu.mum.ezstore.domain.UserCredentials;
import edu.mum.ezstore.shared.DtoValidator;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

/**
 * Created by Sam on 1/23/2017.
 */
class UserTest {
    private DtoValidator<User> validator=new DtoValidator<>();
    private User user=new User();

    @Test
    void testValidInput(){
        user.setLastName("Dov");
        user.setFirstName("Chi");
        user.setAge(25);
        user.setGender('M');

        Address address=new Address();
        address.setStreet("7205");
        address.setCity("Austin");
        address.setState("TX");
        user.setAddress(new ArrayList<Address>(){{add(address);}});

        UserCredentials userCredentials=new UserCredentials();
        userCredentials.setUsername("samqiu");
        userCredentials.setPassword("123456");
        userCredentials.setAuthority("Admin");
        userCredentials.setEnabled(true);
        user.setUserCredentials(userCredentials);

        validator.validate(user);
        validator.assertNoErrors();
    }

    @Test
    void testNameAndAgeInvalid(){
        user.setLastName("");
        user.setFirstName("");
        user.setAge(-1);
        validator.validate(user);
        validator.assertError("firstName");
        validator.assertError("lastName");
        validator.assertError("age");
    }
}
