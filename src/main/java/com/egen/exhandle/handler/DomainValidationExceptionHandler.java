package com.egen.exhandle.handler;

import com.egen.exhandle.exception.DomainValidationException;
import edu.mum.ezstore.config.rest.RestError;
import edu.mum.ezstore.config.rest.ValidationRestError;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

public class DomainValidationExceptionHandler extends ErrorMessageRestExceptionHandler<DomainValidationException> {

	public DomainValidationExceptionHandler() {
		super(UNPROCESSABLE_ENTITY); 
		// TODO Auto-generated constructor stub
	}

	@Override
	public ValidationRestError createBody(DomainValidationException ex, HttpServletRequest req) {

		RestError tmpl = super.createBody(ex, req);

		return new ValidationRestError(tmpl, ex.getErrors());
	}
}
