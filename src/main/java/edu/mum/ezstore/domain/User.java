package edu.mum.ezstore.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class User implements Serializable {

	private static final long serialVersionUID = 21212L;

	private long id;

	@NotEmpty
	@JsonProperty("first_name")
	private String firstName;

	@NotEmpty
    @JsonProperty("last_name")
    private String lastName;

	@Min(value = 1, message="{MinValidation}")
	private Integer age;

	@NotNull
	private Character gender;

	@Valid
	@NotEmpty
	private List<Address> address = new ArrayList<>();

	private List<Item> itemSet = new ArrayList<>();
	
	@Valid
	@NotNull
    @JsonProperty("user_credentials")
    private UserCredentials userCredentials;
	private Set<Comment> comments = new HashSet<>();

	public User(){}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Character getGender() {
		return gender;
	}

	public void setGender(Character gender) {
		this.gender = gender;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@JsonManagedReference // forward reference, serialized
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "user")
	public List<Address> getAddress() {
		return address;
	}
	
	public void setAddress(List<Address> addressSet) {
		this.address = addressSet;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "seller")
	public List<Item> getItemSet() {
		return itemSet;
	}

	public void setItemSet(List<Item> itemSet) {
		this.itemSet = itemSet;
	}

	@JsonBackReference
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "usrCred_id")
	public UserCredentials getUserCredentials() {
		return userCredentials;
	}

	public void setUserCredentials(UserCredentials userCredentials) {
		this.userCredentials = userCredentials;
	}

	@JsonIgnore //ignore for serialize
	@OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "toUser")
	public Set<Comment> getComments() {
		return comments;
	}
	@JsonProperty //enable for deserialize
	public void setComments(Set<Comment> commentSet) {
		this.comments = commentSet;
	}
}
