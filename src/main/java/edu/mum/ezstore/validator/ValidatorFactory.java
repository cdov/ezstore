package edu.mum.ezstore.validator;

import org.springframework.context.ApplicationContext;
import org.springframework.validation.Validator;

import java.util.HashMap;
import java.util.Map;

public class ValidatorFactory {
	private static ValidatorFactory instance;
    private Map<String,Validator> validatorMap=new HashMap<>();
    private ValidatorFactory() {
    	ApplicationContext context=ApplicationContextHolder.getContext();
    	validatorMap.put("User", context.getBean(UserValidator.class));
    }
    
    static {
        instance = new ValidatorFactory();
    }
    public static ValidatorFactory getInstance() {
        return instance;
    }
    
    public Validator getValidator(String key){
        return validatorMap.getOrDefault(key, null);
    }
}
