DELETE FROM `user`;
DELETE FROM `user_credentials`;
DELETE FROM `address`;
DELETE FROM `item`;
DELETE FROM `category`;
DELETE FROM `item_category`;

INSERT INTO `user_credentials` VALUES ('quan','ROLE_USER', '1','$2a$10$0twQoQgdh8VYg4.vj1bBOe4sJqHazyl41ErAq/RN4ohLuHGLcKe9m');
INSERT INTO `user` VALUES (1,24,'Quan','M','Dao','quan');
INSERT INTO `address`  VALUES (1,'fairfield', 'IA', '4th North', '52577',1);

INSERT INTO `user_credentials` VALUES ('sammy','ROLE_ADMIN', '1','$2a$10$0twQoQgdh8VYg4.vj1bBOe4sJqHazyl41ErAq/RN4ohLuHGLcKe9m');
INSERT INTO `user` VALUES (2 ,29,'Chi Proeng','M','Dov','sammy');
INSERT INTO `address` VALUES (2,'fairfield', 'IA', '4th North', '52577',2);

INSERT INTO `user_credentials` VALUES ('friat','ROLE_USER', '1','$2a$10$0twQoQgdh8VYg4.vj1bBOe4sJqHazyl41ErAq/RN4ohLuHGLcKe9m');
INSERT INTO `user` VALUES (3,27,'friat','F','Hadush','friat');
INSERT INTO `address`  VALUES (3,'California', 'LA', '4th North', '53456',3);

INSERT INTO `user_credentials` VALUES ('beimnet','ROLE_ADMIN', '1','$2a$10$0twQoQgdh8VYg4.vj1bBOe4sJqHazyl41ErAq/RN4ohLuHGLcKe9m');
INSERT INTO `user` VALUES (4,29,'Beimnet','M','yimer','beimnet');
INSERT INTO `address`  VALUES (4,'Cansus', 'MO', '4th North', '12345',4);

INSERT INTO `user_credentials` VALUES ('david','ROLE_USER', '1','$2a$10$0twQoQgdh8VYg4.vj1bBOe4sJqHazyl41ErAq/RN4ohLuHGLcKe9m');
INSERT INTO `user` VALUES (5,29,'David','M','Bell','david');
INSERT INTO `address`  VALUES (5,'Salt Lake', 'UT', '4th North', '65784',5);

INSERT INTO `item` (id, name, description, price, order_id, seller_id) VALUES (1, 'bicycle', 'mountain bicycle for a cheap price', 100, NULL, 1);
INSERT INTO `item` (id, name, description, price, order_id, seller_id) VALUES (2, 'iphone 6', 'clean looking iphone 6', 400, NULL, 2);
INSERT INTO `item` (id, name, description, price, order_id, seller_id) VALUES (3, 'laptop', 'Toshiba satellite CORE i3', 500, NULL, 3);
INSERT INTO `item` (id, name, description, price, order_id, seller_id) VALUES (4, 'Washer', 'Pair Maytag washer & dryer. Good condition with little rust spots on bottom of washer.', 500, NULL, 3);
INSERT INTO `item` (id, name, description, price, order_id, seller_id) VALUES (5, 'Air Conditioner', '8,000 b t u air conditioner, works great,80.00,obo', 600, NULL, 4);
INSERT INTO `item` (id, name, description, price, order_id, seller_id) VALUES (6, 'The best of me', 'The book is in a Great condition.', 50, NULL, 5);


INSERT INTO `category` (id, name, description) VALUES (1, 'electronic', 'electronic devices');
INSERT INTO `category` (id, name, description) VALUES (2, 'vehicle', 'car, motorbike, bicycle');
INSERT INTO `category` (id, name, description) VALUES (3, 'appliances', 'refrigirator, Washer, Air Conditioner');
INSERT INTO `category` (id, name, description) VALUES (4, 'books', 'The best of me, Meet in heaven, cook book');

INSERT INTO `item_category` (item_id, category_id) VALUES (1,2);
INSERT INTO `item_category` (item_id, category_id) VALUES (2,1);
INSERT INTO `item_category` (item_id, category_id) VALUES (3,1);
INSERT INTO `item_category` (item_id, category_id) VALUES (4,3);
INSERT INTO `item_category` (item_id, category_id) VALUES (5,3);
INSERT INTO `item_category` (item_id, category_id) VALUES (6,4);


