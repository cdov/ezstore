package edu.mum.ezstore.config;

import edu.mum.ezstore.security.RestAuthenticationAccessDeniedHandler;
import edu.mum.ezstore.security.RestAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author Sam
 *
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	private final RestAuthenticationEntryPoint authenticationEntryPoint;

	private final RestAuthenticationAccessDeniedHandler restAuthenticationAccessDeniedHandler;
	

	private final UserDetailsService customUserDetailsService;

	@Autowired
	public SecurityConfig(RestAuthenticationEntryPoint authenticationEntryPoint,
						  RestAuthenticationAccessDeniedHandler restAuthenticationAccessDeniedHandler,
						  @Qualifier("userDetailsService") UserDetailsService customUserDetailsService) {
		this.authenticationEntryPoint = authenticationEntryPoint;
		this.restAuthenticationAccessDeniedHandler = restAuthenticationAccessDeniedHandler;
		this.customUserDetailsService = customUserDetailsService;
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder());
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**", "/webjars/**"); // #3
	}
	
	/* You cannot set an authentication success handler for BASIC authentication. 
	 * You can, however, extend BasicAuthenticationFilter and override 
	 * onSuccessfulAuthentication and onUnsuccessfulAuthentication method:
	 * http://stackoverflow.com/questions/16734537/spring-security-3-http-basic-authentication-success-handler
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.antMatcher("/**")
			.csrf().disable()
			.cors().and()
			.exceptionHandling()
				.authenticationEntryPoint(authenticationEntryPoint)
				.accessDeniedHandler(restAuthenticationAccessDeniedHandler)
			.and()
			.authorizeRequests()
				.requestMatchers(EndpointRequest.to("info","health","env")).permitAll() //open info, health endpoint
				.requestMatchers(EndpointRequest.toAnyEndpoint()).hasRole("ACTUATOR")  //Other endpoints should require ACTUATOR role
				.requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll() //open static resources access
				.antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')")
				.anyRequest().authenticated()
			.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			.httpBasic();
	}

	/* create bean for AuthenticationManager for autowired in CustomBasicAuthFilter
	 * since the AuthenticationManagerBuilder didn't create AuthenticationManager bean
	 * http://stackoverflow.com/questions/21633555/how-to-inject-authenticationmanager-using-java-configuration-in-a-custom-filter
	 */
	@Bean(name="myAuthenticationManager")
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
       return super.authenticationManagerBean();
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
