package edu.mum.ezstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author cdov
 */
@SpringBootApplication
public class EZStoreApplication {
    public static void main(String[] args) {
        SpringApplication.run(EZStoreApplication.class, args);
    }
}
