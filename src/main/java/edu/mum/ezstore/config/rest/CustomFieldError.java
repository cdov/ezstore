package edu.mum.ezstore.config.rest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"field"})
public class CustomFieldError implements Serializable {

    private static final long serialVersionUID = -3042821267649238101L;

    private String field;
    private Object rejected;
    private String message;

}
