package edu.mum.ezstore.service.impl;

import com.egen.exhandle.exception.ObjectNotFoundException;
import edu.mum.ezstore.aspect.annotation.AnnotationValidation;
import edu.mum.ezstore.domain.Item;
import edu.mum.ezstore.domain.User;
import edu.mum.ezstore.repository.ItemRepository;
import edu.mum.ezstore.service.ItemService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ItemServiceImpl implements ItemService {

	private final ItemRepository itemRepository;

	public ItemServiceImpl(ItemRepository itemRepository) {
		this.itemRepository = itemRepository;
	}

	public List<Item> findAll(){
		return itemRepository.findAll();
	}

	@AnnotationValidation
	public Item save(Item item){
		return itemRepository.save(item);
	}

	public Item findOne(Long id){
		return itemRepository.findById(id).orElseThrow(()-> new ObjectNotFoundException("itemId:"+id));
	}

	@Override
	public List<Item> findByUser(User user) {
		return itemRepository.findBySeller(user);
	}

	public List<Item> findByCategory(Long id) {
		return itemRepository.findByCategory(id);
	}

	@Override
	public List<Item> findByName(String name) {
		return itemRepository.findByName(name);
	}
}
